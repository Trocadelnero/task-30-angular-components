# Angular-Components.
## Description
- Login Form component
- Register Form component
- Inputs: Username, Password
•The Register form should **ALSO** have Confirm Password
•Both components should have a button that emits a string to it’s parent.
•Both components should have an input that receives a string from it’s parent

Try to add the Login form and Register form component to the App Component. Check that it registers and that you can see the message received from the App COmponent and receive a message from the Child component (Login and Register).

```
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

```