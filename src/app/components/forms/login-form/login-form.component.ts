import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @Input() loginFormTitle: string;
  @Output() loginAttempt: EventEmitter<any> = new EventEmitter();

private loginResult: any = {
  isLoggedIn: false,
  message: ''
}

private loginCounter: number = 0;

  constructor() { 
    console.log('LoginFormComponent.constructor()');
  }

  //Auto executed by Angular
  ngOnInit(): void {
    console.log('LoginFormComponent.ngOnInit()');
  }

  onLoginClicked() {

    this.loginCounter++;

    if (this.loginCounter % 2 === 0) {
      this.loginResult.isLoggedIn = true;
      this.loginResult.message = 'You are logged in';
    } else {
      this.loginResult.isLoggedIn = false;
      this.loginResult.message = 'Invalid Login Credentials!';
    }

    this.loginAttempt.emit(this.loginResult);
  }
}
