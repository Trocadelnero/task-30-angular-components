import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  @Input() registerFormTitle: string;
  @Output() registerAttempt: EventEmitter<any> = new EventEmitter();

  private registerResult: any = {
    isRegistered: false,
    message: ''
  }

  private registerCounter: number = 0;

  constructor() {
    console.log('RegisterFormComponent.constructor()');
   }

  ngOnInit(): void {
    console.log('RegisterFormComponent.ngOnInit()');
  }
onRegisterClicked() {
  this.registerCounter++;

  if (this.registerCounter % 2 === 0) {
    this.registerResult.isRegistered = true;
    this.registerResult.message = 'You are now registered';
  } else {
    this.registerResult.isRegistered = false;
    this.registerResult.message = 'Something went wrong'
  }
this.registerAttempt.emit(this.registerResult);
}

}
