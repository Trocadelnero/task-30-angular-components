import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-components';

  

  onRegisterAttempted(registerResult: any): void {
    console.log(registerResult);
    if (registerResult.isRegistered) { //.isRegistered
      //Redirect
    } 
    alert(registerResult.message);
  }

  onLoginAttempted(loginResult: any): void {
    console.log(loginResult);
    if (loginResult.isLoggedIn) { //.isRegistered
      //Redirect
    } 
    alert(loginResult.message);
  }

  
}
